# Create Your Own Personal QA


Bootstrap - First time only
---------------------------

### Create OpenShift projects

Request your OpenShift environments. Go to https://cern.ch/webservices and
create new official websites as PaaS instances (`https://openshift.cern.ch`):

1. Check you have access to CAP QA tags, the shared ImageStreamTag repository:
    `cap-tags.web.cern.ch`.
2. Create you QA environments:
    `cap-qa-[YOUR_GITHUB_USER].web.cern.ch`

### Create DB instance

To create an Postgres instance on CERN DBoD follow this [guide](https://cern.service-now.com/service-portal/article.do?n=KB0001853)

When instance is ready (check also this [DBoD link](https://dbod-user-guide.web.cern.ch/dbod-user-guide/getting_started/postgresql.html) for more info):

1. Connect to the DB from the terminal (credentials for DBOD should be sent to you by mail, when instance is created)
```console
psql -h <ip-alias> -p <port> -U admin
```
2. Create the DB
```console
CREATE DATABASE cap;
```
3. Create the database user that the CAP application will use
```console
CREATE USER invenio WITH PASSWORD 'DB_PASSWORD_HERE';
```
4. Keep credential to add them later into the OpenShift project

### Create ES instance

To create an Elasticsearch instance follow this [guide](./create_es_instance.md)

Keep credential to add them later into the OpenShift project

### Create CERN oAuth credentials

To create oAuth credentials for CERN, go and fill in [this form](https://sso-management.web.cern.ch/OAuth/RegisterOAuthClient.aspx)

1. Add a client_id: `cap-qa-[YOUR_GITHUB_USER]` 
2. redirect_uri: `https://cap-qa-[YOUR_GITHUB_USER].web.cern.ch/oauth/authorized/cern/`
3. Click on "Generate" to generate a secret key"
4. Application Homepage: `https://cap-qa-[YOUR_GITHUB_USER].web.cern.ch`
5. Add description (ex. QA instance for CERN Analysis Preservation) 

### Login on OpenShift

Install the OpenShift [`oc`
client](https://docs.openshift.org/latest/cli_reference/get_started_cli.html)

For all interactions with OpenShift, login and select the right project:

```console
$ oc login https://openshift.cern.ch
Authentication required for https://openshift.cern.ch:443 (CERN)
Username: username
Password:
Login successful.

$ oc project cap-tags
```

### Create OpenShift service accounts

GitLab needs to be able to communicate with OpenShift. The recommended way is
to create a Service Account with limited privileges and use its access token to
import built Docker images and deploy.

There is a KB article that describes the procedure:
<https://cern.service-now.com/service-portal/article.do?n=KB0004553>. Please
refer to it if you encounter difficulties while running the commands below.

> Note
>
> Keep track of these tokens, you will have to add them to GitLab secrets.

Service account for `tags`:

```console
$ oc project cap-tags

# create a service account called `gitlabci-deployer`
$ oc create serviceaccount gitlabci-deployer
serviceaccount "gitlabci-deployer" created

# add `registy-editor` permission to the new account
$ oc policy add-role-to-user registry-editor -z gitlabci-deployer

# We will reffer to the output of this command as `tags-token`
$ oc serviceaccounts get-token gitlabci-deployer
<...tags-token...>
```

Service account for your QA:

```console
$ oc project cap-qa-[YOUR_GITHUB_USER]

$ oc create serviceaccount gitlabci-deployer
serviceaccount "gitlabci-deployer" created

$ oc policy add-role-to-user basic-user -z gitlabci-deployer

# We will reffer to the output of this command as `dev-token`
$ oc serviceaccounts get-token gitlabci-deployer
<...dev-token...>
```

### Share OpenShift project

Allow other projects to pull images from `cap-tags`, to be able to
share the ImageStream with your `QA`.

```console
$ oc project cap-tags
$ oc policy add-role-to-group system:image-puller system:serviceaccounts
```

If you want to restrict this sharing to your projects only, refer to the
[official documentation](https://docs.openshift.org/latest/dev_guide/managing_images.html#allowing-pods-to-reference-images-across-projects).


### Fork this template

First of all, ensure you have access to (or create) the `` GitLab
group. Then:

1. Fork this repo to your group. From now on, you will work in your fork.
2. Change the name and the url of your forked repo. In your repo GitLab page,
    go to `Settings` -> `General` -> `Advanced Settings` -> click on `Expand`
    button and edit `Project name` and `Path` in the `Rename repository`
    section. From now on, we will call it
    [`zenodo-broker-openshift`](https://gitlab.cern.ch/analysispreservation/analysispreservation-openshift).
3. Add some of the secrets/variables. In your repo GitLab page, go to
    `Settings` -> `CI / CD` -> `Secret variables` -> click on `Expand` button
    and create the following variables:
    * `OPENSHIFT_PROJECT_TAGS_NAME`: insert `cap-tags`.
    * `OPENSHIFT_PROJECT_TAGS_TOKEN`: insert the token `<...tags-token...>`.
    * `OPENSHIFT_PROJECT_QA_TOKEN`: insert the token `<...qa-token...>`.
4. Generate a GitLab CI trigger token. In your repo GitLab page, go to
    `Settings` -> `CI / CD` -> `Pipeline triggers` -> click on `Expand` button,
    input a `Description` (for example, `Trigger to create Docker image for
    CAP daily QA`) and edit click the button `Add trigger`. The generated
    token will be used later (don't worry abuot storing it, the creator of the
    token can see it even after leaving the page).


### Create OpenShift deployment

The first time you create `cap-tags`, you need to setup the
ImageStream:

```console
$ oc login https://openshift.cern.ch
$ oc project cap-tags
$ export APPLICATION_IMAGE_NAME='analysispreservationimage-app'
$ export NGINX_UI_IMAGE_NAME='analysispreservationimage-nginx-ui'
$ oc process -f image_stream.yaml \
    --param APPLICATION_IMAGE_NAME='analysispreservationimage-app' \
    --param NGINX_UI_IMAGE_NAME='analysispreservationimage-nginx-ui' | oc create -f -
```

Then, for your `cap-qa-[YOUR_GITHUB_USER]` environment, you will have to follow these steps.

Login and select the right project:

```console
$ oc login https://openshift.cern.ch
$ oc project cap-qa-[YOUR_GITHUB_USER]
```

Create all the needed secrets. Replace username and password in each one.

CAP related scretes:

Copy `secret.env` to `secrets.personal.env` and edit its
secrets for your personal CAP QA instance as needed. Then:

Then do:

```console
$ oc create secret generic cap-secrets --from-env-file=./secrets.personal.env
```

Now add eosclient needed credentials:

```console
$ oc create secret generic eos-credentials --type=eos.cern.ch/credentials --from-literal=keytab-user=XXXX --from-literal=keytab-pwd=XXXX
```

Database password:

```console
$ POSTGRESQL_PASSWORD=$(openssl rand -hex 8)
$ POSTGRESQL_USER=invenio
$ POSTGRESQL_HOST=db
$ POSTGRESQL_PORT=5432
$ POSTGRESQL_DATABASE=invenio
$ oc create secret generic \
  --from-literal="POSTGRESQL_PASSWORD=$POSTGRESQL_PASSWORD" \
  --from-literal="SQLALCHEMY_DB_URI=postgresql+psycopg2://$POSTGRESQL_USER:$POSTGRESQL_PASSWORD@$POSTGRESQL_HOST:$POSTGRESQL_PORT/$POSTGRESQL_DATABASE" \
  db-password
secret "db-password" created
```

RabbitMQ password:

```console
$ RABBITMQ_DEFAULT_PASS=$(openssl rand -hex 8)
$ oc create secret generic \
  --from-literal="RABBITMQ_DEFAULT_PASS=$RABBITMQ_DEFAULT_PASS" \
  --from-literal="CELERY_BROKER_URL=amqp://guest:$RABBITMQ_DEFAULT_PASS@mq:5672/" \
  mq-password
secret "mq-password" created
```

Elasticsearch basic auth:

```console
$ ELASTICSEARCH_PASSWORD=$(openssl rand -hex 8)
$ ELASTICSEARCH_USER=username
$ oc create secret generic \
  --from-literal="ELASTICSEARCH_PASSWORD=$ELASTICSEARCH_PASSWORD" \
  --from-literal="ELASTICSEARCH_USER=$ELASTICSEARCH_USER" \
  elasticsearch-password
```

#### 2. Create and start Invenio services

```console
$ oc process -f services.yaml | oc create -f -
```

#### 3. Init Invenio configuration

Copy `configuration.yaml` to `configuration-dev.yaml` and edit its
configuration parameters as needed. Then:

```console
$ oc process -f configuration-dev.yaml | oc create -f -
```

#### 4. Create Invenio application

For `APPLICATION_IMAGE_TAG`, set the name of the Docker image tag to your Github user name. Note that, since the image has not been built yet, the
application will not be deployed.

```console
$ oc process -f application.yaml \
  --param APPLICATION_IMAGE_NAME='analysispreservationimage-app' \
  --param NGINX_UI_IMAGE_NAME='analysispreservationimage-nginx-ui' \
  --param APPLICATION_IMAGE_TAG='[YOUR_GITHUB_USER]' \
  --param TAGS_PROJECT='cap-tags' | oc create -f -
```

### Build

Start a build of your application code on the specified release.

```console
$ build.sh -t v1.0.3
```

This step will trigger a GitLab-CI pipeline which will:

1. build the Invenio Docker file image. It clones the tag `v1.0.3` of
    <https://github.com/zenodo/zenodo-broker.git>
2. push the built image to the GitLab registry:
    <https://gitlab-registry.cern.ch/zenodogroup/zenodo-broker-openshift/zenodobrokerimage:v1.0.3>
3. import the new built image metadata to the `zenodo-broker-tags` OpenShift
    project. With this step, the image will be available to all
    `zenodo-broker-*` OpenShift projects and it will be ready to be deployed.

#### Automation

You can setup automatic building, for example at the end of a Travis build,
sending a POST request to GitLab-CI. Use the `curl` request that you can find
at the end of the script `build.sh` (or call the `build.sh` file).

#### Deploy

```console
$ deploy.sh dev v1.0.3
```

This step will:

1. for the `dev` project, it will update the OpenShift ImageStreamTag called
    `dev` to point to the image `v1.0.3`. Something like a symlink.
2. deploy the `dev` tag in the `dev` environment.

#### Automation

When sending the POST request to the GitLab-CI to build a new image, you can
pass an extra parameter `deploy=dev`.

## Customize OpenShift deployment configuration

If you need to customize any of the OpenShift deployment configuration, it is
recommended to edit the templates yaml files and then replace any existing
configuration.

For example:

```console
$ oc process -f services.yaml | oc replace -f -
```
