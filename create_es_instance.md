# ES on OpenStack with `docker-compose`

## Setup VM

### 1. Create a VM
Following http://clouddocs.web.cern.ch/ using CLI or web interface create CC7 VM with enough RAM to ES.

### 2. SSH into the VM

Log into `lxplus`:
```console
$ ssh <username>@lxplus-cloud.cern.ch
> ssh root@<vm_name>.cern.ch
```

### 3. Install requirements

Once inside install requirements:

```console
# following https://www.elastic.co/guide/en/elasticsearch/reference/5.2/docker.html#docker-cli-run-prod-mode
echo 262144 > /proc/sys/vm/max_map_count
yum -y update
yum install python-pip httpd-tools -y
pip install --upgrade pip setuptools wheel
pip install docker-compose
sudo yum remove docker docker-common docker-selinux docker-engine
sudo yum install -y yum-utils device-mapper-persistent-data lvm2
sudo yum-config-manager --add-repo \
     https://download.docker.com/linux/centos/docker-ce.repo
sudo yum install docker-ce -y
sudo systemctl start docker
sudo curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```

NOTE: Docker installation taken from official [documentation](https://docs.docker.com/engine/installation/linux/docker-ce/centos/#install-docker-ce-1) for CentOS7.

## Create and Run ES with reverse proxy

```console
$ mkdir elasticsearch
$ cd elasticsearch
```

### 1. Reverse proxy with `nginx`

Create ssl certificates:

```console
$ sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 \
  -keyout nginx.key -out nginx.crt
...
```

Create `.htpasswd`:
```console
$ htpasswd -c .htpasswd cap
New password:
Re-type new password:
Adding password for user cap
```

Adding `nginx.conf`:
```nginx=
server {
    listen 443 ssl;
    server_name "elascticsarch-nginx";
    charset utf-8;

    # We could whitelist all openshift VMs
    # allow 192.168.1.1/24;
    # deny  all;

    ssl_certificate     /etc/ssl/nginx.crt;
    ssl_certificate_key /etc/ssl/nginx.key;

    auth_basic "COD3 Authentication";
    auth_basic_user_file conf/.htpasswd;

    location / {
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header Host $host;
        proxy_pass http://elasticsearch:9200;
    }
}
```

Create a `Dockerfile` for nginx:
```dockerfile
FROM nginx

RUN rm /etc/nginx/conf.d/default.conf
ADD nginx.conf /etc/nginx/conf.d/

ADD .htpasswd /etc/nginx/conf/.htpasswd
ADD nginx.crt /etc/ssl/nginx.crt
ADD nginx.key /etc/ssl/nginx.key
```

Create a `docker-compose.yml` file:
```yaml
version: "2"

services:
  elasticsearch:
    restart: "always"
    image: elasticsearch:5
    command: ["elasticsearch", "-E", "logger.org.elasticsearch.deprecation=error"]
    environment:
      - bootstrap.memory_lock=true
      - "ES_JAVA_OPTS=-Xms512m -Xmx512m"
    volumes:
      - elasticsearch_data:/usr/share/elasticsearch/data
    ports:
      - "9200"
      - "9300"

  elasticsearch-proxy:
    restart: "always"
    depends_on:
      - elasticsearch
    build: .
    ports:
      - "443:443"
    links:
      - elasticsearch

volumes:
  elasticsearch_data:

```


## Running the service

After changing `docker-compose` options:
```console
$ docker-compose up -d
```

## Applying updates

After changing `docker-compose` options:
```console
$ docker-compose down
$ docker-compose up -d
```

Data will remain since it is inside `elasticsearch_data` volume.